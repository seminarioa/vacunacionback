package co.edu.uniajc.vacunacion.controller;

import co.edu.uniajc.vacunacion.dto.EnfermeroDTO;
import co.edu.uniajc.vacunacion.model.EnfermeroModel;
import co.edu.uniajc.vacunacion.service.LoginService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/login")
public class LoginController {

    private LoginService loginService;

    @Autowired
    public LoginController(LoginService loginService){
        this.loginService = loginService;
    }

    @PostMapping(path = "/autenticacion")
    @Operation(summary = "login for enfermero")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = EnfermeroModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public EnfermeroDTO findUserAndPassword(@RequestBody EnfermeroModel enfermerosModel){
        return loginService.findUserAndPassword(enfermerosModel.getUsuario(), enfermerosModel.getContrasena());
    }
}
