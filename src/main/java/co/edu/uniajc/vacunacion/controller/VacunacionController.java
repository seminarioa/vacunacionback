package co.edu.uniajc.vacunacion.controller;

import co.edu.uniajc.vacunacion.model.VacunacionModel;
import co.edu.uniajc.vacunacion.service.VacunacionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/vacunacion")
public class VacunacionController {

    private VacunacionService VacunacionService;

    @Autowired
    public VacunacionController(VacunacionService VacunacionService){
        this.VacunacionService = VacunacionService;
    }

    @PostMapping(path = "/save")
    @Operation(summary = "Insert VacunacionModel")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = VacunacionModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public VacunacionModel createVacunacion(@RequestBody VacunacionModel vacunacionModel){
        return VacunacionService.createVacunacion(vacunacionModel);
    }

    @PutMapping(path = "/update")
    @Operation(summary = "Update VacunacionModel")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = VacunacionModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public VacunacionModel updateVacunacion(@RequestBody VacunacionModel vacunacionModel){
        return VacunacionService.updateVacunacion(vacunacionModel);
    }

    @DeleteMapping(path = "/delete")
    @Operation(summary = "Delete VacunacionModel by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = VacunacionModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public void deleteVacunacion(@RequestParam(name = "id") Long id){
        VacunacionService.deleteVacunacion(id);
    }

    @GetMapping(path = "/all")
    @Operation(summary = "Find all VacunacionModel")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = VacunacionModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public List<VacunacionModel> findAllVacunacion(){
        return VacunacionService.findAllVacunacion();
    }

}
