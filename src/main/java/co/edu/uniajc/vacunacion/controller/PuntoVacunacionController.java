package co.edu.uniajc.vacunacion.controller;

import co.edu.uniajc.vacunacion.model.PuntoVacunacionModel;
import co.edu.uniajc.vacunacion.service.PuntoVacunacionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/puntovacunacion")

public class PuntoVacunacionController {

    private PuntoVacunacionService puntoVacunacionService;

    @Autowired
    public PuntoVacunacionController(PuntoVacunacionService puntoVacunacionService){
        this.puntoVacunacionService = puntoVacunacionService;
    }

    @PostMapping(path = "/save")
    @Operation(summary = "Insert puntovacunacion ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = PuntoVacunacionModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public PuntoVacunacionModel createPuntoVacunacion(@RequestBody PuntoVacunacionModel puntoVacunacionModel){
        return puntoVacunacionService.createPuntoVacunacion(puntoVacunacionModel);
    }

    @PutMapping(path = "/update")
    @Operation(summary = "Update puntovacunacion")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = PuntoVacunacionModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public PuntoVacunacionModel updatepuntoVacunacion(@RequestBody PuntoVacunacionModel puntoVacunacionModel){
        return puntoVacunacionService.updatePuntoVacunacion(puntoVacunacionModel);
    }

    @DeleteMapping(path = "/delete")
    @Operation(summary = "Delete puntovacunacion by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = PuntoVacunacionModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public void deletepuntoVacunacion(@RequestParam(name = "id") Long id){
        puntoVacunacionService.deletePuntoVacunacion(id);
    }

    @GetMapping(path = "/all")
    @Operation(summary = "Find all puntovacunacion")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = PuntoVacunacionModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public List<PuntoVacunacionModel> findAllPacientes(){
        return puntoVacunacionService.findAllPuntoVacunacion();
    }


}