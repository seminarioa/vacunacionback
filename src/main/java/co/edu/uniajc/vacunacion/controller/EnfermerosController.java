package co.edu.uniajc.vacunacion.controller;

import co.edu.uniajc.vacunacion.dto.EnfermeroDTO;
import co.edu.uniajc.vacunacion.model.EnfermeroModel;
import co.edu.uniajc.vacunacion.service.EnfermerosService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/enfermeros")
public class EnfermerosController {

    private EnfermerosService enfermerosService;

    @Autowired
    public EnfermerosController(EnfermerosService enfermerosService){
        this.enfermerosService = enfermerosService;
    }

    @PostMapping(path = "/save")
    @Operation(summary = "Insert nurses")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = EnfermeroModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public EnfermeroModel createEnfermeros(@RequestBody EnfermeroModel enfermerosModel){
        return enfermerosService.createEnfermeros(enfermerosModel);
    }

    @PutMapping(path = "/update")
    @Operation(summary = "Update nurses")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = EnfermeroModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public EnfermeroModel updateEnfermeros(@RequestBody EnfermeroModel enfermerosModel){
        return enfermerosService.updateEnfermeros(enfermerosModel);
    }

    @DeleteMapping(path = "/delete")
    @Operation(summary = "Delete nurses by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = EnfermeroModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public void deleteEnfermeros(@RequestParam(name = "id") Long id){
        enfermerosService.deleteEnfermeros(id);
    }

    @GetMapping(path = "/all")
    @Operation(summary = "Find all nurses")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = EnfermeroModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public List<EnfermeroModel> findAllEnfermeros(){
        return enfermerosService.findAllEnfermeros();
    }

}
