package co.edu.uniajc.vacunacion.controller;

import co.edu.uniajc.vacunacion.model.PacienteModel;
import co.edu.uniajc.vacunacion.service.PacientesService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/pacientes")
public class PacientesController {

    private PacientesService pacientesService;

    @Autowired
    public PacientesController(PacientesService pacientesService){
        this.pacientesService = pacientesService;
    }

    @PostMapping(path = "/save")
    @Operation(summary = "Insert pacientes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = PacienteModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public PacienteModel createPacientes(@RequestBody PacienteModel pacientesModel){
        return pacientesService.createPacientes(pacientesModel);
    }

    @PutMapping(path = "/update")
    @Operation(summary = "Update pacientes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = PacienteModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public PacienteModel updatePacientes(@RequestBody PacienteModel pacientesModel){
        return pacientesService.updatePacientes(pacientesModel);
    }

    @DeleteMapping(path = "/delete")
    @Operation(summary = "Delete pacientes by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = PacienteModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public void deletePacientes(@RequestParam(name = "id") Long id){
        pacientesService.deletePacientes(id);
    }

    @GetMapping(path = "/all")
    @Operation(summary = "Find all pacientes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = PacienteModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public List<PacienteModel> findAllPacientes(){
        return pacientesService.findAllPacientes();
    }


    @GetMapping(path = "/getPaciente")
    @Operation(summary = "Find all pacientes by numero_documento")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = PacienteModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public PacienteModel findByDocumento(@RequestParam(name = "documento")String documento ){
        return pacientesService.findByDocumento(documento);
    }


}
