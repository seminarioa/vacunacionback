package co.edu.uniajc.vacunacion.controller;

import co.edu.uniajc.vacunacion.model.InventarioModel;
import co.edu.uniajc.vacunacion.service.InventarioService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/invenatrio")

public class InventarioController {

    private InventarioService InventarioService;

    @Autowired
    public InventarioController(InventarioService InventarioService){
        this.InventarioService = InventarioService;
    }

    @PostMapping(path = "/save")
    @Operation(summary = "Insert InventarioModel")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = InventarioModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public InventarioModel createInventario(@RequestBody InventarioModel inventarioModel){
        return InventarioService.createInventario(inventarioModel);
    }

    @PutMapping(path = "/update")
    @Operation(summary = "Update InventarioModel")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = InventarioModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public InventarioModel updateInventario(@RequestBody InventarioModel inventarioModel){
        return InventarioService.updateInventario(inventarioModel);
    }

    @DeleteMapping(path = "/delete")
    @Operation(summary = "Delete InventarioModel by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = InventarioModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public void deleteInventario(@RequestParam(name = "id") Long id){
        InventarioService.deleteInventario(id);
    }

    @GetMapping(path = "/all")
    @Operation(summary = "Find all InventarioModel")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = InventarioModel.class))
            }),
            @ApiResponse(responseCode = "400", description = "Something Went Wrong", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content),
    })
    public List<InventarioModel> findAllInventario(){
        return InventarioService.findAllInventario();
    }


}
