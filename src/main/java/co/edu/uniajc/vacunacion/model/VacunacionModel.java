package co.edu.uniajc.vacunacion.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="vacunacion")
public class VacunacionModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "va_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "va_id_paciente")
    private PacienteModel paciente;

    @ManyToOne
    @JoinColumn(name = "va_id_enfermero")
    private EnfermeroModel enfermero;

    @Column(name = "va_cantidad_dosis")
    @Size(min = 1, max = 15)
    @NotNull
    private Long dosis;

    @Column(name = "va_fabricante")
    @Size(min = 1, max = 50)
    @NotNull
    private String fabricante;

    @Column(name = "va_fecha_aplicacion")
    @NotNull
    private Date fechaAplicacion;

    @Column(name = "va_ips_vacunadora")
    @Size(min = 1, max = 30)
    @NotNull
    private String ips;

    @Column(name = "va_lote")
    @Size(min = 1, max = 10)
    @NotNull
    private String lote;
}
