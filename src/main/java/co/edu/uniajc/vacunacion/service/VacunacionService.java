package co.edu.uniajc.vacunacion.service;

import co.edu.uniajc.vacunacion.model.VacunacionModel;
import co.edu.uniajc.vacunacion.repository.VacunacionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VacunacionService {

    private final VacunacionRepository vacunacionRepository;

    @Autowired
    public VacunacionService(VacunacionRepository vacunacionRepository){
        this.vacunacionRepository = vacunacionRepository;
    }

    public VacunacionModel createVacunacion(VacunacionModel vacunacionModel) {
        return this.vacunacionRepository.save(vacunacionModel);
    }

    public VacunacionModel updateVacunacion(VacunacionModel vacunacionModel) {
        return this.vacunacionRepository.save(vacunacionModel);
    }

    public void deleteVacunacion(Long id) {
        this.vacunacionRepository.deleteById(id);
    }

    public List<VacunacionModel> findAllVacunacion() {
        return this.vacunacionRepository.findAll();
    }

}
