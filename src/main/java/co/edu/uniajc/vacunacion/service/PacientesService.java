package co.edu.uniajc.vacunacion.service;

import co.edu.uniajc.vacunacion.model.PacienteModel;
import co.edu.uniajc.vacunacion.repository.PacientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PacientesService {

    private final PacientesRepository pacientesRepository;

    @Autowired
    public PacientesService(PacientesRepository pacientesRepository){
        this.pacientesRepository = pacientesRepository;
    }

    public PacienteModel createPacientes(PacienteModel pacientesModel) {
        return this.pacientesRepository.save(pacientesModel);
    }

    public PacienteModel updatePacientes(PacienteModel pacientesModel) {
        return this.pacientesRepository.save(pacientesModel);
    }

    public void deletePacientes(Long id) {
        this.pacientesRepository.deleteById(id);
    }

    public List<PacienteModel> findAllPacientes() {
        return this.pacientesRepository.findAll();
    }

    public PacienteModel findByDocumento(String documento){
        return pacientesRepository.findByDocumento(documento);
    }

}
